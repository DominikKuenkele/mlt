#!/bin/bash
wc -l ${2}/* | head -n -1 | sort -r | head -n $1 | sed -e 's/^[ ]*[0-9]* //' | paste - <(wc -l ${3}/* | head -n -1 | sort -r | head -n $1 | sed -e 's/^[ ]*[0-9]* //')
