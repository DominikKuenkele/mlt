#!/bin/bash

number=$1

while [ -n "$2" ]
do
    if [ $(wc -l < "$2") -gt $number ]
    then
        echo "$2"
    fi
    shift
done
