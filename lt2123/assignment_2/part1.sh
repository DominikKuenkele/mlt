#!/bin/bash

for file in $2
do  
    if [ $(wc -l < $file) -gt $1 ]
    then
        echo $file
    fi
done
