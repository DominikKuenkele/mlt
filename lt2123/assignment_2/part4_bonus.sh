#!/bin/bash
echo $0

if [ -n "$4" ]
then
    dest="$4"
    src="$3"
else
    dest="$3"
    src="."
fi

mkdir -p $dest
((word_number=${1}-1))
while IFS= read -r word
do
    grep -nrE "(( |([[:punct:]])+ )?\w+( |( [[:punct:]])+ )){${word_number},}\b${word}\b" ${src}/* | awk -v line=${2} -F ':' '$2>line{print $1}' | uniq | xargs -I % cp -f --parents % $dest
done < <(grep "" /dev/stdin)