#!/bin/bash

mkdir -p $4
((word_number=${1}-1))
while IFS= read -r word
do
    grep -nrE "(( |([[:punct:]])+ )?\w+( |( [[:punct:]])+ )){${word_number},}\b${word}\b" ${3}/* | awk -v line=${2} -F ':' '$2>line{print $1}' | uniq | xargs -I % cp -f --parents % $4
done < <(grep "" /dev/stdin)