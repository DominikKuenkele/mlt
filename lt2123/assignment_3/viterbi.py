from HMM import HMM


def viterbi(observations, hmm):
    observations = [observation.lower() for observation in observations]
    viterbi = {}
    backpointer = {}
    for state in hmm.states:
        viterbi[state, 0] = hmm.initial_probabilities[state] + hmm.emission_probabilities[state, observations[0]]
        backpointer[state, 0] = 0
    for time_step in range(1, len(observations)):
        for state in hmm.states:
            max_prob = max([(viterbi[prev_state, time_step - 1]
                             + hmm.transition_probabilities[prev_state, state]
                             + hmm.emission_probabilities[state, observations[time_step]],
                             prev_state)
                            for prev_state in hmm.states])
            viterbi[state, time_step] = max_prob[0]
            backpointer[state, time_step] = max_prob[1]

    last_steps = {(state, time): prob + hmm.transition_probabilities[state, HMM.Symbols.EOS]
                  for (state, time), prob in viterbi.items()
                  if time == len(observations) - 1}
    best_last_step = max(last_steps, key=last_steps.get)

    best_path = [best_last_step[0]]
    for time_step in range(len(observations) - 1, 0, -1):
        best_path.append(backpointer[best_path[-1], time_step])
    best_path.reverse()
    return best_path, viterbi[best_last_step]


if __name__ == '__main__':
    hmm_smoothed = HMM('en_ewt-ud-train.100k.toks_tags', transition_k=0.15, emission_k=0.1)
    sentences = ['The children run quickly .',
                 'Do many cities have beautiful parks ?',
                 'The music sounds great , when played by professional artists .']
    for sentence in sentences:
        print(sentence)
        print(viterbi(sentence.split(' '), hmm_smoothed))
        print()

    # The children run quickly .
    # (['DET', 'NOUN', 'VERB', 'ADV', 'PUNCT'], -31.495873059919894)
    #
    # Do many cities have beautiful parks ?
    # (['AUX', 'ADJ', 'NOUN', 'AUX', 'ADJ', 'NOUN', 'PUNCT'], -51.33306184457682)
    #
    # The music sounds great , when played by professional artists .
    # (['DET', 'NOUN', 'VERB', 'ADJ', 'PUNCT', 'SCONJ', 'VERB', 'ADP', 'ADJ', 'NOUN', 'PUNCT'], -75.33836161128366)
