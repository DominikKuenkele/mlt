from enum import Enum

from CondPr import LogCondPr


class HMM:
    class Symbols(Enum):
        SOS = '<SOS>'
        EOS = '<EOS>'

    def __init__(self, filename, transition_k=0., emission_k=0.):
        transitions = {}
        emissions = {}
        self.states = set()

        with open(filename, encoding='utf-8') as file:
            for sentence in file:
                assignments = sentence.rstrip().split(' ')
                last_tag = self.Symbols.SOS
                for assignment in assignments:
                    word, tag = assignment.split('|')
                    word = word.lower()
                    emissions[tag, word] = emissions.get((tag, word), 0) + 1
                    transitions[last_tag, tag] = transitions.get((last_tag, tag), 0) + 1
                    last_tag = tag
                    self.states.add(tag)
                transitions[last_tag, self.Symbols.EOS] = transitions.get((last_tag, self.Symbols.EOS), 0) + 1

        self.transition_probabilities = LogCondPr(transitions, transition_k)
        self.emission_probabilities = LogCondPr(emissions, emission_k)
        self.initial_probabilities = {tag: self.transition_probabilities[start, tag]
                                      for start, tag in self.transition_probabilities.probability_distribution
                                      if start == self.Symbols.SOS}


if __name__ == '__main__':
    hmm_non_smoothed = HMM('en_ewt-ud-train.100k.toks_tags')
    hmm_smoothed = HMM('en_ewt-ud-train.100k.toks_tags', transition_k=0.15, emission_k=0.1)

    transitions = [('ADJ', 'NOUN'),
                   ('DET', 'NOUN'),
                   ('ADJ', 'ADJ')]
    print('===Transitions===')
    for left, right in transitions:
        try:
            print(f"non-smoothed:\tlog Pr({right}|{left})={hmm_non_smoothed.transition_probabilities[left, right]}")
        except Exception as e:
            print(e)
        print(f"smoothed:\t\tlog Pr({right}|{left})={hmm_smoothed.transition_probabilities[left, right]}")

    emissions = [('CCONJ', 'questions'),
                 ('ADJ', 'hello'),
                 ('PUNCT', '.')]
    print('===Emissions===')
    for left, right in emissions:
        try:
            print(f"non-smoothed:\tlog Pr({right}|{left})={hmm_non_smoothed.emission_probabilities[left, right]}")
        except Exception as e:
            print(e)
        print(f"smoothed:\t\tlog Pr({right}|{left})={hmm_smoothed.emission_probabilities[left, right]}")

    # ===Transitions===
    # non-smoothed:	log Pr(NOUN|ADJ)=-0.6586374096591885
    # smoothed:		log Pr(NOUN|ADJ)=-0.6590101272427803
    # non-smoothed:	log Pr(NOUN|DET)=-0.5217352841719175
    # smoothed:		log Pr(NOUN|DET)=-0.5220439899387862
    # non-smoothed:	log Pr(ADJ|ADJ)=-2.8287376825993826
    # smoothed:		log Pr(ADJ|ADJ)=-2.828762635743178
    # ===Emissions===
    # non-smoothed:	log Pr(questions|CCONJ)=-inf
    # smoothed:		log Pr(questions|CCONJ)=-10.685194910075554
    # non-smoothed:	log Pr(hello|ADJ)=-inf
    # smoothed:		log Pr(hello|ADJ)=-11.241562836385416
    # non-smoothed:	log Pr(.|PUNCT)=-0.9875334492827235
    # smoothed:		log Pr(.|PUNCT)=-1.0836913881539838
