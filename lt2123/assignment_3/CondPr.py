import itertools
import math


class AbstrCondPr:
    def __init__(self, counts, k=0.):
        self.k = k
        self._lefts = set()
        self._rights = set()
        total_counts = {}
        for (left, right), count in counts.items():
            total_counts[left] = total_counts.get(left, 0) + count
            self._lefts.add(left)
            self._rights.add(right)

        self.probability_distribution = {}
        all_combinations = list(itertools.product(self._lefts, self._rights))
        for left, right in all_combinations:
            numerator = (counts.get((left, right), 0) + k)
            denominator = (total_counts[left] + (k * len(self._rights)))
            self.probability_distribution[left, right] = numerator / denominator

    def __getitem__(self, item):
        if not isinstance(item, tuple) or len(item) != 2:
            raise KeyError('Key must be a 2-tuple')
        left, right = item

        if left not in self._lefts:
            raise KeyError(f'{left} was not observed')
        if (left, right) not in self.probability_distribution.keys():
            raise KeyError(f'{left} and {right} were not observed together')

        return self.probability_distribution[left, right]

    def show(self):
        raise NotImplementedError


class CondPr(AbstrCondPr):
    def __init__(self, counts, k=0.):
        super().__init__(counts, k)

    def show(self):
        for (left, right), probability in self.probability_distribution.items():
            print(f"Pr('{right}'|'{left}') = {probability:0.3f}")


class LogCondPr(AbstrCondPr):
    def __init__(self, counts, k=0.):
        if isinstance(counts, CondPr):
            self.k = counts.k
            self.probability_distribution = counts.probability_distribution.copy()
        else:
            super().__init__(counts, k)

        for distr, prob in self.probability_distribution.items():
            if prob == 0:
                self.probability_distribution[distr] = float('-inf')
            else:
                self.probability_distribution[distr] = math.log(prob)

    def show(self):
        for (left, right), probability in self.probability_distribution.items():
            print(f"log Pr('{right}'|'{left}') = {probability:0.3f}")
