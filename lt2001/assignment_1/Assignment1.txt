# Q1
# This command will list all files beginning with "chapter", followed by a number and any suffix. It is flexible and would also include newly added chapters like "chapter23.txt" or "chapter59.txt"
ls chapter[0-9]*

# Q2
# With this command all files beginning with "chapter" are searched for Dinah.
grep Dinah chapter*

# Q3
# This command lists the number of instances in all selected files from Q2
grep -c Dinah chapter*

# Q4
# lists number of occurences of Alice in each chapter 
grep -c Alice chapter[0-9]*
# lists number of words of each chapter
wc -w chapter[0-9]*
# by dividing the number of occurences by the number of words for each chapter you get the answer to both questions:
# Chapter 9 with arround 0,022447 occurences of Alice per word (51/2272)

# Q5
# Since all paragraphs are one line, the number of lines can be counted to get the number of paragraphs
wc -l tokenized.txt

# Q6
# adding "> one_token_per_line" will output of the command into this file
tr ' ' '\n' <tokenized.txt > one_token_per_line

# Q7
# lists the last five tokens of the sorted file
sort one_token_per_line | tail -n5
”
”
”
”
”

# Q8
# No, the output of the command from Q6 could be piped into the command of Q7. E.g.:
tr ' ' '\n' <tokenized.txt | sort one_token_per_line | tail -n5
# Pro
# - more readable commands
# - can be debugged easily (when using pipes, the result of the first commands cannot be checked)
# Con
# - duplicate files (more storage needed; files are out of sync, when original file is adapted)

# Q9
# The option -r is needed to reverse the output. Thus, the first ten combinatios (head -n10) are the most common instead of least common. Instead, we could also use "tail -n10" to get the most common in reversed order.
# The option -n indicates that the sort algorithm should interpret multi digit numbers (e.g. 2418) as whole number and not only use the first digit for sorting. The normal behaviour would result in e.g. the following order: 1; 2; 2418; 3; 31; 4; ... 
# In our case the option -n would not be necessary, since the numbers of the "uniq" command are intended by a space for each missing digit. Therefore, the "sort" command will sort the four digit, three digit, two digit and one digit numbers each by their own.

# Q10
# "tail -n+2 one_token_per_line" will output the lines from the file one_token_per_line starting from line 2 (with "| head -n5" line 2 to 6)
# "head -n5 one_token_per_line" would output the first 5 lines starting from the first line

# Q11
# adding "tail -n+11" will output all lines starting from line 11 and adding "head -n10" will then output the first 10 lines from the previous output (lines 11-20)
paste one_token_per_line one_token_per_line_o1 | sort | uniq -c | sort -nr | tail -n+11 | head -n10

# Q12
# This will copy the orignal file into a new one with an offset of two lines
tail -n+3 one_token_per_line > one_token_per_line_o2

# Now the same command as in the PDF, but extended with the third file, can be used to show the most common 20 triplets
$ paste one_token_per_line one_token_per_line_o1 one_token_per_line_o2 | sort | uniq -c | sort -nr | head -n20
    216 ,       ’       said
    203 ’       said    the
    115 ’       said    alice
    110 .       ’       ‘
     69 .       ‘       i
     65 !       ’       said
     59 ,       ’       the
     54 *       *       *
     53 the     mock    turtle
     50 alice   .       ‘
     48 !       ’       ‘
     47 ’       ‘       i
     44 ?       ’       ‘
     42 ?       ’       said
     42 ,       and     the
     39 said    alice   ,
     35 ,       and     she
     33 said    alice   .
     32 ,       you     know
     31 ,       ’       she


# Q13
# This command will sort for the most common triplets, exclude all lines including punctuations (^.*[.,!’‘:;*()”“?-) and output the most common 20 triplets
$ paste one_token_per_line one_token_per_line_o1 one_token_per_line_o2 | sort | uniq -c | sort -nr | grep -v '^.*[.,!’‘:;*()”“?\-].*$' | head -n20
     53 the     mock    turtle
     30 the     march   hare
     29 said    the     king
     21 the     white   rabbit
     20 said    the     hatter
     19 said    to      herself
     19 said    the     mock
     18 said    the     caterpillar
     17 she     went    on
     17 she     said    to
     17 said    the     gryphon
     17 as      she     could
     15 said    the     duchess
     15 one     of      the
     14 said    the     cat
     12 said    the     queen
     12 minute  or      two
     11 there   was     a
     11 said    to      the
     11 out     of      the

# Using the following regex would also include e.g. apostrophes in the words like "i do n't" or hyphens as in "so out-of-the-way down"
$ paste one_token_per_line one_token_per_line_o1 one_token_per_line_o2 | sort | uniq -c | sort -nr | grep -P '^\s*[0-9]{1,3}\s(\w+[^\s]*\w*|\w*[^\s]+\w+)\s*(\w+[^\s]*\w*|\w*[^\s]+\w+)\s*(\w+[^\s]*\w*|\w*[^\s]+\w+)$' | head -n20
     53 the     mock    turtle
     30 the     march   hare
     30 i       do      n’t
     29 said    the     king
     21 the     white   rabbit
     20 said    the     hatter
     19 said    to      herself
     19 said    the     mock
     18 said    the     caterpillar
     17 she     went    on
     17 she     said    to
     17 said    the     gryphon
     17 as      she     could
     15 said    the     duchess
     15 one     of      the
     15 i       ca      n’t
     14 said    the     cat
     12 said    the     queen
     12 minute  or      two
     11 wo      n’t     you