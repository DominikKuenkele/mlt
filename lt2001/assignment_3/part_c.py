try:
    stick_1 = int(input("length of stick 1:"))
    stick_2 = int(input("length of stick 2:"))
    stick_3 = int(input("length of stick 3:"))

    if (
            stick_1 > stick_2 + stick_3 or
            stick_2 > stick_1 + stick_3 or
            stick_3 > stick_1 + stick_2
    ):
        print('No')
    else:
        print('Yes')
except ValueError:
    print('Please enter a number')
