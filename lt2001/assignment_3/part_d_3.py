string = 'mainly in the plain'
vowels = ('a', 'e', 'i', 'o', 'u', 'y')
outvalue = 0

for character in string:
    if character in vowels:
        outvalue += 1

print(outvalue)
