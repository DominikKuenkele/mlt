# Example of binary search (with a problem...)
# LT2001 HT20 University of Gothenburg

whole_deck = 'bcdefgh'
my_card = 'a'

print('Looking for card', my_card,'among', whole_deck)

top = len(whole_deck)
bottom = 0
middle = 0                                                  # variable needs to be declared before use

while top != bottom:                                        # when top equals bottom, the card is not in the deck and the loop would be endless
    print('bottom =', bottom, 'top =', top,
          '- remaining cards', whole_deck[bottom:top])
    middle = (top+bottom)//2
    if whole_deck[middle] == my_card:
        break
    elif my_card < whole_deck[middle]:
        top = middle
    else:
        # my_card > whole_deck[middle]
        bottom = middle+1

if top != bottom:                                           # card is in deck
    print('Card', my_card, 'is at position', middle)
else:                                                       # card is not in deck
    print('Card not in deck')
