string = ['m', 'a', 'i', 'n', 'l', 'y', ' ', 'i', 'n', ' ', 't', 'h', 'e', ' ', 'p', 'l', 'a', 'i', 'n']
vowels = ['a', 'e', 'i', 'o', 'u', 'y']
output = []

for character in string:
    if character in vowels:
        output.append(1)
    else:
        output.append(0)

print(output)
