# Part A
## 1
1. The type of 3.25 is 'float' and the type of 4 is 'int'. The type of the whole expression is 'float'.
2. 'bool'
3. Both strings have the type 'str', so the whole expression has also the type 'str' with a length of 10
4. The type of this expression is 'list'. It consists of four numbers with the type 'int'
5. The type is 'list' consisting of 5 items. The first is a 'list' with 2 items, the second an 'int', the third a 'float' and the fifth an 'int'
6. THe type is 'list' with on item. This item has the type 'str' with a length of 4.

## 2
1. Name of month: type 'str'. The name of a month is a string of characters, so the type 'str' will fit best
2. Number of week: type 'int'. The number of a week is always an integer value, so the type 'int' will fit best
3. Record temperatures for each day of a month: type 'list' consisting of 28-31 items with the type 'float'. Temperatures can have decimal places, so it should have the type 'float'. 'int' would also be possible, when the temperature should only be saved in integer numbers. All single values can be then saved in list for each month with 28-31 items, depending on the month.
4. Mean temperature in a month: type 'float'. The mean temperature will definitely have decimal places, so we need the type 'float'.

# Part B
## 1
a = 3
b = 4                                                   # b needs to be an int to perform arithmetic operations
c = 5
print(a**2, "plus " + str(b**2), "equals " + str(c**2)) # to concatenate an int to a string, it first needs to be cast to a string

## 2
some_list = [4,5,6,7,8,9]
print(some_list[0])       # a_list is not existing; to access a value of a list, the index needs to be an 'int'

## 3
a = 3
b = 4
c = (a**2 + b**2)**0.5    # the variable must be on the left side of an assignment
print(c)

## 4
my_list = [23, "hi", 2.4, None, ['blipblop']]   # names of list must be the same
for item in my_list:
    print(item)

## 5
num = 24
a = 34.999
result = num*(13/a**2)+1.0
print("Result:", result)     # arguments need to be separated by a ,

## 6
# program has no errors
letters = ['a','b','c']
letters[1] = 'x'

## 7
letters = ['a, b, c', None]  # letters needs to be a list with at least 2 values that it can be accessed with the index 1
letters[1] = 'x'

# Part C
try:
    stick_1 = int(input("length of stick 1:"))
    stick_2 = int(input("length of stick 2:"))
    stick_3 = int(input("length of stick 3:"))

    if (
            stick_1 > stick_2 + stick_3 or
            stick_2 > stick_1 + stick_3 or
            stick_3 > stick_1 + stick_2
    ):
        print('No')
    else:
        print('Yes')
except ValueError:
    print('Please enter a number')


# Part D
## 1
This is 'map', since we want to apply a function to each item in the list and generate a list with the same length

string = ['m', 'a', 'i', 'n', 'l', 'y', ' ', 'i', 'n', ' ', 't', 'h', 'e', ' ', 'p', 'l', 'a', 'i', 'n']
vowels = ['a', 'e', 'i', 'o', 'u', 'y']
output = []

for character in string:
    if character in vowels:
        output.append(1)
    else:
        output.append(0)

print(output)


## 2
This is a reduce operation.

inlist = [10, 20, 30, 40]
outvalue = inlist[0]

for number in inlist[1:]:
    outvalue += number

print(outvalue)


## 3
This is also a reduce operation, since we only get on output value. The input can also be a 'str' or a tuple. In the below example, I used a 'str' as an input and a tuple for the vowels.

string = 'mainly in the plain'
vowels = ('a', 'e', 'i', 'o', 'u', 'y')
outvalue = 0

for character in string:
    if character in vowels:
        outvalue += 1

print(outvalue)


## 4
string = ['mainly','in','the','plain']
vowels = ('a', 'e', 'i', 'o', 'u', 'y')
outlist = []

for word in string:
    outvalue = 0
    for chararcter in word:
        if chararcter in vowels:
            outvalue += 1
    outlist.append(outvalue)

print(outlist)


## 5
inlist = range(931,960)
outlist = []

for number in inlist:
    if (number % 5 == 0) ^ (number % 7 == 0):
        outlist.append(number)

print(outlist)


## 6
s = [1,2,3,4]
t = []
i = 0

while i < len(s):
    t = [s[i]]+t
    i += 1

print(s)
print(t)


## 7
s = [1,2,3,4,6]
t = []

for i in s:
    t = [i] + t

print(s)
print(t)


# Part E
Explanation from previous assignment:
No variable ('top', 'bottom', 'middle') is changing its value after the 4th run in the loop, since the program cannot find the letter in the middle. This will result in an endless loop.
All three variables have the same value (2). This won't change, since the program will always jump into the 'elif'-block, because 'whole_deck[2]' is 'd' and 'c' is "lower" than 'd'. A fix would be, to check if 'top' equals 'bottom' as condition of the while loop. If that is the case, the deck does not contain the card.

# Example of binary search (with a problem...)
# LT2001 HT20 University of Gothenburg

whole_deck = 'bcdefgh'
my_card = 'a'

print('Looking for card', my_card,'among', whole_deck)

top = len(whole_deck)
bottom = 0
middle = 0                                                  # variable needs to be declared before use

while top != bottom:                                        # when top equals bottom, the card is not in the deck and the loop can be exited
    print('bottom =', bottom, 'top =', top,
          '- remaining cards', whole_deck[bottom:top])
    middle = (top+bottom)//2
    if whole_deck[middle] == my_card:
        break
    elif my_card < whole_deck[middle]:
        top = middle
    else:
        # my_card > whole_deck[middle]
        bottom = middle+1

if top != bottom:                                           # card is in deck
    print('Card', my_card, 'is at position', middle)
else:                                                       # card is not in deck
    print('Card not in deck')
