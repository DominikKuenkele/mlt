"""The module provides an enum for the weight of a prediction model

Course: LT2001
Date: 26-10-2021
"""

from enum import Enum


class PredictionModelWeight(Enum):
    WEIGHTED = True
    NOT_WEIGHTED = False
