"""This module provides functions to read files

Course: LT2001
Date: 26-10-2021

read_reference_data() -- returns the content of a given file as reference data
read_evaluation_data() -- returns the content of a given file as evaluation data
"""


def read_reference_data(filename):
    """Reads a file and returns its content as a reference data dictionary

    The file needs to be in the following structure (each element separated by a tab symbol):
    user_id song_id song_rating song_id song_rating ...
    user_id song_id song_rating song_id song_rating ...
    ...

    Args:
        filename: file, which shall be read

    Returns:
        A dictionary of the user histories in the following structure:
        {user_id: {song_id: song_rating, song_id: song_rating}, user_id: {...}, ...}.
    """
    user_histories = {}
    with open(filename) as file:
        for line in file:
            if line.strip() != '':
                user_data = line.strip().split('\t')
                user_id = user_data.pop(0)
                user_histories[user_id] = {}
                while len(user_data) > 0:
                    song_id = user_data.pop(0)
                    song_rating = int(user_data.pop(0))
                    user_histories[user_id][song_id] = song_rating
    return user_histories


def read_evaluation_data(data_file):
    """Reads a file and returns its content as a list of 4-tuples.

    The file needs to be in the following structure (each element separated by a tab symbol):
    user_id song_id song_rating song_id song_rating ...
    user_id song_id song_rating song_id song_rating ...
    ...

    Args:
        data_file: file, which shall be read

    Returns:
        A list of 4-tuples for each user. The tuples have the following format:
        (user_id, user_history, last_song_id, last_song_rating)
        The last song is NOT included in the user history.
    """
    user_evaluation = []
    with open(data_file) as file:
        for line in file:
            if line.strip() != '':
                user_data = line.strip().split('\t')
                user_id = user_data.pop(0)

                last_song_rating = int(user_data.pop(-1))
                last_song_id = user_data.pop(-1)

                user_history = {}
                while len(user_data) > 0:
                    song_id = user_data.pop(0)
                    song_rating = int(user_data.pop(0))
                    user_history[song_id] = song_rating

                user_evaluation.append((user_id, user_history, last_song_id, last_song_rating))
    return user_evaluation
