"""This module provides helper functions, used in both the pass and high pass assignment.

Course: LT2001
Date: 26-10-2021

Some functions in the recommendation process are used exactly the same in
the pass and high pass assignment. To prevent duplicate code, those functions
were collected in this module.

Functions:

get_complete_history() -- returns a merged dictionary for two user history files
get_song_catalogue() -- returns a set with all songs for given user histories
get_best_predictions() -- returns a dictionary of songs with the best score and confidence
"""

from collections import defaultdict
from pathlib import Path

import file_handler
from Custom_Exceptions import ArgumentError

_MAX_NUMBER_OF_PREDICTIONS = 3


def get_complete_history(user_histories_file, user_evaluation_file):
    """Returns a dictionary, containing user histories from two files.

    Args:
        user_histories_file: file, containing user histories.
        user_evaluation_file: file, containing user histories.

    Returns:
        A dict mapping user ids to their user history. Each user history is another dictionary,
          mapping song ids to the users song rating.
    """
    user_histories = file_handler.read_reference_data(user_histories_file)
    user_histories_2 = file_handler.read_reference_data(user_evaluation_file)

    merged_user_histories = defaultdict(dict)
    merged_user_histories.update(user_histories)
    for user, history in user_histories_2.items():
        merged_user_histories[user].update(history)

    return merged_user_histories


def get_song_catalogue(user_histories):
    """Returns a set of all songs in the user histories.

    Args:
     user_histories: a dictionary containing user histories in the following format:
       {user_id: {song_id: song_rating, song_id: song_rating}, user_id: {...}, ...}.

    """
    song_catalogue = set()
    for user in user_histories:
        for song in user_histories[user]:
            song_catalogue.add(song)
    return sorted(song_catalogue)


def get_best_predictions(user_predictions):
    """Returns a dictionary of songs with the best score and confidence.

    Args:
        user_predictions: A dictionary mapping song ids to their prediction.
          The prediction is a tuple of score and confidence.
    """
    # exclude 'bad' predictions
    # user_predictions = {
    #     song_id: prediction
    #     for song_id, prediction in user_predictions.items()
    #     if prediction[0] >= 0
    # }

    # sort predictions by 1. score and 2. confidence
    user_predictions = [
        (song_id, prediction)
        for song_id, prediction in
        sorted(user_predictions.items(), key=lambda x: (x[1][0], x[1][1]), reverse=True)
    ]

    return user_predictions[:_MAX_NUMBER_OF_PREDICTIONS]


def check_arguments(arguments):
    """Checks command line arguments on their validity.

    Args:
        arguments: sys.argv arguments when calling the script.

    Raises:
        ArgumentError: At least one of the arguments was not valid.
    """
    if len(arguments) < 4:
        raise ArgumentError('Please specify minimum three arguments!')
    if not (Path(arguments[1]).is_file() and Path(arguments[2]).is_file()):
        raise ArgumentError('Please specify files as the first two arguments!')
