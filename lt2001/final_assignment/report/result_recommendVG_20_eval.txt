| Evaluation of Predictions |
-----------------------------
+--------------------------------------+---------------------+------------+
| model                                | correct predictions | evaluation |
+--------------------------------------+---------------------+------------+
| (1 reference user(s), not weighted)  | 22/50               | 44.0%      |
| (5 reference user(s), not weighted)  | 26/50               | 52.0%      |
| (10 reference user(s), not weighted) | 26/50               | 52.0%      |
| (5 reference user(s), weighted)      | 26/50               | 52.0%      |
| (10 reference user(s), weighted)     | 25/50               | 50.0%      |
+--------------------------------------+---------------------+------------+

| Recommendation for Users |
----------------------------
The following model was used: (5 reference user(s), not weighted)
Based on the the users history, the following song recommendations were generated:

|u3733570
+----------+-----------------+----------------------+
| song id  | predicted score | confidence           |
+----------+-----------------+----------------------+
| s13567   | 2               | 0.08886426375707067  |
| s83168   | 2               | 0.08886426375707067  |
| s85376   | 2               | 0.08886426375707067  |
+----------+-----------------+----------------------+
