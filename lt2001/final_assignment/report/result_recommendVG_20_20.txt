| Evaluation of Predictions |
-----------------------------
+--------------------------------------+---------------------+------------+
| model                                | correct predictions | evaluation |
+--------------------------------------+---------------------+------------+
| (1 reference user(s), not weighted)  | 1356/1356           | 100.0%     |
| (5 reference user(s), not weighted)  | 1037/1356           | 76.47%     |
| (10 reference user(s), not weighted) | 1002/1356           | 73.89%     |
| (5 reference user(s), weighted)      | 1341/1356           | 98.89%     |
| (10 reference user(s), weighted)     | 1336/1356           | 98.53%     |
+--------------------------------------+---------------------+------------+

| Recommendation for Users |
----------------------------
The following model was used: (1 reference user(s), not weighted)
Based on the the users history, the following song recommendations were generated:

|u3733570
+----------+-----------------+----------------------+
| song id  | predicted score | confidence           |
+----------+-----------------+----------------------+
| s5205516 | 2               | 0.09740465098831574  |
| s13567   | 2               | 0.08886426375707067  |
| s1973933 | 2               | 0.08886426375707067  |
+----------+-----------------+----------------------+
