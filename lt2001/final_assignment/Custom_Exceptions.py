"""This module contains classes for custom exceptions in the song recommendation

Course: LT2001
Date: 24-10-2021

"""


class ArgumentError(Exception):
    """Command line arguments for a script are not valid"""
    pass
