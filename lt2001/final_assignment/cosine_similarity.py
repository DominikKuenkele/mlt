"""This module provides functions to calculate the cosine similarity of two dictionaries

Course: LT2001
Date: 23-10-2021

dot() -- returns the dot product of two dictionaries
cosim() -- returns the cosine similarity of two dictionaries
"""

import math


def dot(h1, h2):
    """Returns the dot product of two dictionaries

    Calculates the sum of h1[key] * h2[key] for all keys found in both dictionaries.
    If no common keys were found, the sum is 0.

    Args:
        h1: a dictionary mapping keys to number values.
        h2: a dictionary mapping keys to number values.
    """
    dot_product = 0
    for song_id in h1:
        if song_id in h2.keys():
            dot_product += int(h1[song_id]) * int(h2[song_id])
    return dot_product


def cosim(h1, h2):
    """Returns the cosine similarity of two dictionaries

    Args:
        h1: a dictionary mapping keys to number values.
        h2: a dictionary mapping keys to number values.
    """
    dot_product = dot(h1, h2)
    dot_product_h1 = dot(h1, h1)
    dot_product_h2 = dot(h2, h2)

    return dot_product / (math.sqrt(dot_product_h1) * (math.sqrt(dot_product_h2)))
