"""This module contains a script, to make song recommendations based on the listening history of users.

Course: LT2001
Date: 27-10-2021

This script contains the code for the 'high pass' assignment.
The recommendation process consists of two steps. In the first step, the quality of multiple recommendation models
will be evaluated. This is done, by making predictions for each model on already listened songs and comparing the
results to the actual like score.
Afterwards, the script makes song recommendations for users, based on reference user histories and their similarity
to the user using the model with teh best predictions.

"""
import sys

import file_handler
from Custom_Exceptions import ArgumentError
from PredictionModelWeight import PredictionModelWeight
from cosine_similarity import cosim
from output_recommend import format_evaluation, format_recommendation
from recommend_helper import get_complete_history, get_song_catalogue, get_best_predictions, check_arguments

_MODELS = [(1, PredictionModelWeight.NOT_WEIGHTED),
           (5, PredictionModelWeight.NOT_WEIGHTED),
           (10, PredictionModelWeight.NOT_WEIGHTED),
           (5, PredictionModelWeight.WEIGHTED),
           (10, PredictionModelWeight.WEIGHTED)]


def predict_like(reference_data, user_similarities, song_id, model):
    """Predicts the like score of a song for given user similarities, a model and multiple reference histories.

    The model defines, how the prediction is made. It consists of two parts:
    Number of reference users: How many similar reference users shall be taken into account?
    Weighted: Shall the reference user be weighted by their similarity? More similar users have a higher impact on the
      predicted score.

    Args:
        reference_data: A dictionary mapping user ids to the users history (see format in other parameter).
          This is the basis for calculating the user similarity.
        user_similarities: A dictionary mapping reference user ids to the similarity of a given user.
        song_id: The song, for which the prediction is made
        model: The model, describing the approach, how to predict the like.
          It is a tuple of the number of reference users and the PredictionModelWeight
    Returns:
        A tuple consisting of the predicted like score and the confidence (similarity to reference user)
    """
    number_of_reference_users = model[0]
    weighted = model[1] == PredictionModelWeight.WEIGHTED
    histories_with_song = {
        reference_user_id: reference_user_history
        for reference_user_id, reference_user_history in reference_data.items()
        if song_id in reference_user_history.keys()
    }

    if histories_with_song == {}:
        prediction = (0, 1.0)
    else:
        user_similarities = {
            reference_user: similarity
            for reference_user, similarity in user_similarities.items()
            if reference_user in histories_with_song.keys()
        }
        user_similarities = sorted(user_similarities.items(), key=lambda user_sim: user_sim[1],
                                   reverse=True)[:number_of_reference_users]
        prediction = _calculate_weighted_predictions(user_similarities, histories_with_song, song_id, weighted)

    return prediction


def _calculate_weighted_predictions(user_similarities, user_histories, song_id, weighted):
    """Calculates the predictions based on the model.

    Args:
        user_similarities: The users and their similarity that will be taken into account for the prediction.
        user_histories: The user histories of the users from the user similarities.
        song_id: The song, for which the prediction is made.
        weighted: A boolean if the reference user shall be weighted by their similarity
    Returns: A tuple consisting of the predicted like score and the confidence (similarity to reference user)

    """
    weight = 1.0
    total_weight = 0.0
    total_score = 0.0
    total_confidence = 0.0
    for user_id, user_similarity in user_similarities:
        if weighted:
            weight = user_similarity

        total_weight += weight
        total_score += weight * user_histories[user_id][song_id]
        total_confidence += user_similarity

    # if total_weight is 0, total_score would also be zero. This would result in a division by zero error
    if total_weight == 0:
        average_score = _round_to_like(0)
    else:
        average_score = _round_to_like(total_score / total_weight)
    average_confidence = total_confidence / len(user_similarities)

    return average_score, average_confidence


def _round_to_like(average_score):
    if average_score < 0:
        return -1
    elif average_score < 1.5:
        return 1
    else:
        return 2


def evaluate(user_histories_file, user_evaluation_file, models):
    """Evaluates how good the predictions are for different models.

    For this, the function makes predictions for already listened songs for multiple users
    and compares the results to the actual like scores. This is done for each provide model.

    Args:
        user_histories_file: file of user histories, which is used as reference for the prediction.
        user_evaluation_file: file of user histories, for which the predictions should be made.
        models: A list of models, for which the evaluation will be done.
          The models are tuples of the number of reference users and the PredictionModelWeight
    Returns:
        A dictionary, mapping the model to a tuple of the number of correct predictions and total predictions.
    """
    evaluations = {}
    reference_history = file_handler.read_reference_data(user_histories_file)
    user_evaluation = file_handler.read_evaluation_data(user_evaluation_file)

    for model in models:
        evaluation_score = 0
        for user_data in user_evaluation:
            user_history = user_data[1]
            song_id = user_data[2]
            user_like = user_data[3]

            user_similarities = _calculate_user_similarities(reference_history, user_history)
            prediction = predict_like(reference_history, user_similarities, song_id, model)
            if prediction[0] == user_like:
                evaluation_score += 1
        evaluations[model] = (evaluation_score, len(user_evaluation))

    return evaluations


def _calculate_user_similarities(reference_data, user_history):
    return {
        reference_user: cosim(user_history, reference_user_history)
        for reference_user, reference_user_history in reference_data.items()
    }


def recommend(user_histories_file, user_evaluation_file, user_ids, model):
    """Recommends songs for all user ids based on a reference user history.

    This function predicts for each user the like scores for all songs in the user histories,
    the user has not listened to. Out of this, the function retrieves the best predictions based on
    first like score and second confidence.
    The like scores are predicted using the given model.

    Args:
        user_histories_file: file of user histories, which is used as reference for the prediction.
        user_evaluation_file: another file of user histories, which is used as reference for the prediction.
        user_ids: users, for which the recommendations should be made.
        model: The model, describing the approach, how to predict the like.
          It is a tuple of the number of reference users and the PredictionModelWeight
    Returns:
        A dictionary, mapping users to the predicted songs with their like score and confidence.
    """
    complete_history = get_complete_history(user_histories_file, user_evaluation_file)
    song_catalogue = get_song_catalogue(complete_history)

    predictions = {}
    for user in user_ids:
        listened_songs = complete_history[user].keys()
        user_similarities = _calculate_user_similarities(complete_history, complete_history[user])
        user_predictions = {
            song: predict_like(complete_history, user_similarities, song, model)
            for song in song_catalogue
            if song not in listened_songs
        }
        predictions[user] = get_best_predictions(user_predictions)

    return predictions


def main(user_histories_file, user_evaluation_file, user_ids):
    """Makes first an evaluation of the predictions and secondly song recommendations for the given users.

    During the evaluation, multiple models for the predictions are compared and the best is used for the recommendation.
    The results of the evaluation and the recommendations are printed out in a readable format.

    Args:
        user_histories_file: file of user histories, which is used as reference for the prediction.
        user_evaluation_file: file of user histories, for which the predictions should be made.
          This file will also be used as reference in the recommendation
        user_ids: users, for which the recommendations should be made.
    """
    evaluations = evaluate(user_histories_file, user_evaluation_file, _MODELS)
    print(format_evaluation(evaluations))

    print('')

    # retrieves model with most correct predictions
    best_model = max(evaluations.items(), key=lambda model_evaluation: model_evaluation[1][0])[0]
    recommendation = recommend(user_histories_file, user_evaluation_file, user_ids, best_model)
    print(format_recommendation(recommendation, best_model))


if __name__ == '__main__':
    try:
        check_arguments(sys.argv)
        main(sys.argv[1], sys.argv[2], sys.argv[3:])
    except ArgumentError as e:
        sys.stderr.write(str(e))
