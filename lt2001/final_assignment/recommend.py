"""This module contains a script, to make song recommendations based on the listening history of users.

Course: LT2001
Date: 24-10-2021

This script contains the code for the 'pass' assignment.
The recommendation process consists of two steps. In the first step, the quality of the recommendation
will be evaluated. This is done, by making predictions on already listened songs and comparing the results to
the actual like score.
Afterwards, the script makes song recommendations for users, based on reference user histories and their similarity
to the user.

"""

import sys

import file_handler
from Custom_Exceptions import ArgumentError
from PredictionModelWeight import PredictionModelWeight
from cosine_similarity import cosim
from output_recommend import format_evaluation, format_recommendation
from recommend_helper import get_complete_history, get_song_catalogue, get_best_predictions, check_arguments


def predict_like(reference_data, user_history, song_id):
    """Predicts the like score of a song for a given user history and multiple reference histories.

    This is done in two steps. Firstly, the most similar reference user, who has heard the song is calculated by
    using the cosine similarity.
    Secondly, the like score of this reference user is retrieved and predicted as the new like score.

    Args:
        reference_data: A dictionary mapping user ids to the users history (see format in other parameter).
          This is the basis for calculating the user similarity.
        user_history: A dictionary mapping song ids to a like score.
          This is the history of the user, the prediction should be made for
        song_id: The song, for which the prediction is made
    Returns:
        A tuple consisting of the predicted like score and the confidence (similarity to reference user)
    """
    histories_with_song = {
        reference_user_id: reference_user_history
        for reference_user_id, reference_user_history in reference_data.items()
        if song_id in reference_user_history.keys()
    }

    if histories_with_song == {}:
        prediction = (0, 1.0)
    else:
        user_similarities = {
            reference_user: cosim(user_history, reference_user_history)
            for reference_user, reference_user_history in histories_with_song.items()
        }
        closest_user = max(user_similarities, key=lambda user_id: user_similarities[user_id])
        prediction = histories_with_song[closest_user][song_id], user_similarities[closest_user]

    return prediction


def evaluate(user_histories_file, user_evaluation_file):
    """Evaluates how good the predictions are.

    For this, the function makes predictions for already listened songs for multiple users
    and compares the results to the actual like scores.

    Args:
        user_histories_file: file of user histories, which is used as reference for the prediction.
        user_evaluation_file: file of user histories, for which the predictions should be made.
    Returns:
        A tuple of the number of correct predictions and total predictions.
    """
    evaluation_score = 0

    reference_history = file_handler.read_reference_data(user_histories_file)
    user_evaluation = file_handler.read_evaluation_data(user_evaluation_file)

    for user_data in user_evaluation:
        user_history = user_data[1]
        song_id = user_data[2]
        user_like = user_data[3]

        prediction = predict_like(reference_history, user_history, song_id)
        if prediction[0] == user_like:
            evaluation_score += 1
    return evaluation_score, len(user_evaluation)


def recommend(user_histories_file, user_evaluation_file, user_ids):
    """Recommends songs for all user ids based on a reference user history.

    This function predicts for each user the like scores for all songs in the user histories,
    the user has not listened to. Out of this, the function retrieves the best predictions based on
    first like score and second confidence.

    Args:
        user_histories_file: file of user histories, which is used as reference for the prediction.
        user_evaluation_file: another file of user histories, which is used as reference for the prediction.
        user_ids: users, for which the recommendations should be made.
    Returns:
        A dictionary, mapping users to the predicted songs with their like score and confidence.
    """
    complete_history = get_complete_history(user_histories_file, user_evaluation_file)
    song_catalogue = get_song_catalogue(complete_history)

    predictions = {}
    for user in user_ids:
        listened_songs = complete_history[user].keys()
        user_predictions = {
            song: predict_like(complete_history, complete_history[user], song)
            for song in song_catalogue
            if song not in listened_songs
        }
        predictions[user] = get_best_predictions(user_predictions)

    return predictions


def main(user_histories_file, user_evaluation_file, user_ids):
    """Makes first an evaluation of the predictions and secondly song recommendations for the given users.

    The results of the evaluation and the recommendations are printed out in a readable format.

    Args:
        user_histories_file: file of user histories, which is used as reference for the prediction.
        user_evaluation_file: file of user histories, for which the predictions should be made.
          This file will also be used as reference in the recommendation
        user_ids: users, for which the recommendations should be made.
    """
    evaluation = evaluate(user_histories_file, user_evaluation_file)
    # model needs to be added to reuse the function from the high pass assignment
    print(format_evaluation({(1, PredictionModelWeight.NOT_WEIGHTED): evaluation}))

    print('')

    recommendation = recommend(user_histories_file, user_evaluation_file, user_ids)
    print(format_recommendation(recommendation))


if __name__ == '__main__':
    try:
        check_arguments(sys.argv)
        main(sys.argv[1], sys.argv[2], sys.argv[3:])
    except ArgumentError as e:
        sys.stderr.write(str(e))
