"""This module filters out the necessary data for the recommendation program out of the piki_dataset

Course: LT2001
Date: 22-10-2021

This script can be run from the command line e.g. as following:
    python .\filter_piki.py 20 <.\materials\piki_dataset.csv > .\materials\piki_th20.histories

parse_user_history() -- returns the filtered data in a specific format as a string
parse_csv_file() -- returns a filtered piki_dataset.csv as a dictionary
"""

import sys
from collections import defaultdict

from Custom_Exceptions import ArgumentError


def main(threshold):
    """Prints the filtered piki_dataset to the commandline in a specific format.

    The function reads the stdin for the piki_dataset.

    Args:
        threshold: minimum number of songs, a user must have listened to
    """
    csv_file = sys.stdin.read()
    parsed_user_data = parse_csv_file(csv_file)

    parsed_user_data = {
        user_id: parsed_user_data[user_id]
        for user_id in parsed_user_data
        if len(parsed_user_data[user_id]) >= threshold
    }

    print(parse_user_history(parsed_user_data))


def parse_user_history(user_data):
    """Returns the piki_dataset in the following format:

    user_id song_id song_rating song_id son_rating ...
    user_id song_id song_rating song_id son_rating ...
    user_id song_id song_rating ...

    Each item is separated by a tab symbol.

    Args:
        user_data: A dictionary mapping a user id to a list of song scores.
          The song scores are tuples of song id and song rating
    """
    output = ''
    for user_id in user_data:
        output += user_id
        user_history = user_data[user_id]
        for song_rating in user_history:
            output += '\t' + song_rating[0] + '\t' + str(song_rating[1])
        output += '\n'
    return output


def parse_csv_file(csv_file):
    """Reads a the comma separated piki_dataset and returns the data as dictionary.

    The function filters out the needed data (user_id, song_ids and song_ratings).
    Furthermore, it turns a rating of 0 into -1 for a better processing.

    Args:
        csv_file: A string of the comma separated piki_dataset.
    """
    users = defaultdict(list)
    lines = csv_file.strip().split('\n')
    for line in lines[1:]:
        user_data = line.split(',')
        user_id = 'u' + user_data[1]
        song_id = 's' + user_data[2]
        score = int(user_data[3])
        if score == 0:
            score = -1

        users[user_id].append((song_id, score))

    return users


def _check_arguments(arguments):
    if len(arguments) != 2:
        raise ArgumentError('Please specify exactly one additional argument!')

    try:
        int(arguments[1])
    except ValueError:
        raise ArgumentError('Please specify a number as an argument!')


if __name__ == '__main__':
    try:
        _check_arguments(sys.argv)
        main(int(sys.argv[1]))
    except ArgumentError as e:
        sys.stderr.write(str(e))
