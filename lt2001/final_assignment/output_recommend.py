"""This module provide functions, to output results to the terminal

Course: LT2001
Date: 28-10-2021


format_evaluation() -- returns a formatted string for given evaluation results
format_recommendation() -- returns a formatted string for given recommendation results
"""

from PredictionModelWeight import PredictionModelWeight


def format_evaluation(evaluations):
    """Returns a formatted string for the evaluation results.

    Args:
        evaluations: A dictionary, mapping a model to the evaluation of the model.
          The model is a tuple, consisting of the number of reference users and if those users are weighted
          The evaluation is a tuple, consisting of the number of correct predictions and total predictions
    """
    output = '| Evaluation of Predictions |' \
             '\n-----------------------------\n'

    columns = [('model', 36), ('correct predictions', 19), ('evaluation', 10)]

    data = []
    for model, evaluation in evaluations.items():
        model = '({close_users} reference user(s), {weighted})' \
            .format(close_users=model[0],
                    weighted='weighted' if model[1] == PredictionModelWeight.WEIGHTED else 'not weighted')
        predictions = str(evaluation[0]) + '/' + str(evaluation[1])
        percent = str(round(evaluation[0] / evaluation[1] * 100, 2)) + '%'

        data.append([model, predictions, percent])

    output += _format_table(columns, data)
    return output


def format_recommendation(recommendation, model=(1, PredictionModelWeight.NOT_WEIGHTED)):
    """Returns a formatted string for the recommendation results.

    Args:
        recommendation: A dictionary, mapping a user id to the predicted songs for the user.
          The predicted songs are a list of tuples, consisting of the song id and its prediction
        model: A tuple, consisting of the number of reference users and if those users are weighted
    """
    columns = [('song id', 8), ('predicted score', 15), ('confidence', 20)]

    output = '| Recommendation for Users |' \
             '\n----------------------------' \
             '\nThe following model was used: '
    model_string = '({close_users} reference user(s), {weighted})'
    output += model_string.format(close_users=model[0],
                                  weighted='weighted' if model[1] == PredictionModelWeight.WEIGHTED else 'not weighted')

    output += '\nBased on the the users history, the following song recommendations were generated:'

    for user, songs in recommendation.items():
        output += '\n\n|' + user

        data = [(song_id, str(prediction[0]), str(prediction[1])) for song_id, prediction in songs]
        output += '\n' + _format_table(columns, data)

    return output


def _format_table(columns, data):
    """Returns a formatted table for given input data.

    Args:
        columns: A list of tuples, consisting of the name of the column header and its length
        data: A list of tuples for each row, containing the data for each column
    """
    table = '+'
    table += _get_table_separator(columns)
    table += '\n|'
    for column_name, width in columns:
        table += ' ' + column_name + (width - len(column_name) + 1) * ' ' + '|'
    table += '\n+'
    table += _get_table_separator(columns)

    for row in data:
        table += '\n|'
        for column_index in range(0, len(row)):
            table += ' ' + str(row[column_index]) + (columns[column_index][1] - len(row[column_index]) + 1) * ' ' + '|'

    table += '\n+'
    table += _get_table_separator(columns)

    return table


def _get_table_separator(columns):
    separator = ''
    for column_name, width in columns:
        separator += (width + 2) * '-' + '+'
    return separator
