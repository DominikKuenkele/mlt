database = {
    ('Gerlof', 'von Bayes'): {
        'social security number': '4567',
        'birthday': (2016, 12, 31),
        'interests': ['syntax', 'historical linguistics', 'logic programming and spelling']
    },
    ('Anna', 'Lang'): {
        'social security number': '2345',
        'birthday': (2015, 8, 14),
        'interests': ['argumentation analysis']
    },
    ('Pi', 'Thon-Rappel'): {
        'social security number': '8901',
        'birthday': (2015, 2, 29),
        'interests': ['programming language design']
    },
    ('Amelie', 'Poulain'): {
        'social security number': '0123',
        'birthday': (2015, 3, 3),
        'interests': ['NLP for Parisian French', 'sand castle engineering']
    }
}

# 5.1
interests = database[('Gerlof', 'von Bayes')]['interests']
print(interests)

# 5.2
outlist = []
for name in database:
    if name[1].startswith('L'):
        first_name = name[0]
        birthday = database[name]['birthday']
        social_security_number = database[name]['social security number']
        outlist.append((first_name, birthday, social_security_number))

print(outlist)

# 6
# Update Amelie
database[('Amelie', 'Poulain')]['birthday'] = (2001, 4, 25)
print(database[('Amelie', 'Poulain')])

# Update Gerlof
database[('Gerlof', 'von Bayes')]['interests'].remove('logic programming and spelling')
print(database[('Gerlof', 'von Bayes')])
