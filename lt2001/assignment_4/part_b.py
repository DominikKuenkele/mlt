text = ['to','be',',','or','not','to','be',',','that','is','the','question',':']

# 1
bigrams_1 = {}
for index in range(0, len(text)-1):
    bigram = (text[index], text[index + 1])
    if bigram in bigrams_1.keys():
        bigrams_1[bigram] += 1
    else:
        bigrams_1[bigram] = 1

print(bigrams_1)

# 2
bigrams_2 = {}
for bigram in bigrams_1:
    first_key = bigram[0]
    second_key = bigram[1]
    count = bigrams_1[bigram]
    if first_key in bigrams_2.keys():
        bigrams_2[first_key][second_key] = count
    else:
        bigrams_2[first_key] = {second_key: count}

print(bigrams_2)

# 3
bigrams_3 = {}
for word in bigrams_2:
    second_dic = bigrams_2[word]
    for second_word in second_dic:
        bigram = (word, second_word)
        bigrams_3[bigram] = second_dic[second_word]

print(bigrams_3)

# 4
text_trigrams = ['can', 'you', 'can', 'a', 'can', 'as', 'a', 'canner', 'can', 'can', 'a', 'can','?']
trigrams_4 = {}
for index in range(0, len(text_trigrams)-2):
    trigram = (text_trigrams[index], text_trigrams[index + 1], text_trigrams[index + 2])
    if trigram in trigrams_4.keys():
        trigrams_4[trigram] += 1
    else:
        trigrams_4[trigram] = 1

print(trigrams_4)

# 5
# extends the code from # 4
trigrams_5 = trigrams_4.copy()
for trigram in trigrams_5:
    bigram = (trigram[0], trigram[1])
    count = 0
    for trigram4 in trigrams_4:
        bigram4 = (trigram4[0], trigram4[1])
        if bigram4 == bigram:
            count += trigrams_4[trigram4]
    trigrams_5[trigram] = trigrams_5[trigram] / count

print(trigrams_5)
