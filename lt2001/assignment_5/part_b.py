# 1
def trimmed_max(xs, y):
    number = xs[0]
    for x in xs:
        if y > x > number:
            number = x
    return number if number < y else None


if __name__ == '__main__':
    print(trimmed_max([-3, 5, -0.1, -100, 1, 5000], -200))


# 2
def normalize_spaces(text):
    text = trim_text(text)
    if len(text) != 0:
        output_text = remove_duplicate_spaces(text)
    else:
        output_text = ''
    return output_text


def trim_text(text):
    while text.startswith(' '):
        text = text[1:]
    while text.endswith(' '):
        text = text[:-1]
    return text


def remove_duplicate_spaces(text):
    output_text = text[0]
    for index in range(1, len(text)):
        if text[index] == ' ':
            if text[index - 1] != ' ':
                output_text += text[index]
        else:
            output_text += text[index]
    return output_text


if __name__ == '__main__':
    print(normalize_spaces(" What's up,    doc?     "))


# 3
def alphanum_tokens(tokens):
    tokens_dupl = tokens.copy()
    for token in tokens_dupl:
        if not token.isalnum():
            tokens.remove(token)


if __name__ == '__main__':
    t = ['Nr', '37', ':', 'So', 'excited', '!', '/', 'The', 'Pointer', 'Sisters']
    alphanum_tokens(t)
    print(t)
