import sys


def sticks_and_triangles(arguments):
    if len(arguments) == 3:
        lengths = cast_inputs(arguments)
        evaluate_triangle(lengths)
    elif len(arguments) == 0:
        lengths = get_input()
        evaluate_triangle(lengths)
    else:
        print('Please part_c.py with no or exactly three arguments. Kthxbye!')


def cast_inputs(arguments):
    lengths = []
    try:
        for argument in arguments:
            lengths.append(int(argument))
        return lengths
    except ValueError:
        print('Please enter a number')


def get_input():
    lengths = []
    try:
        lengths.append(int(input("length of stick 1:")))
        lengths.append(int(input("length of stick 2:")))
        lengths.append(int(input("length of stick 3:")))
        return lengths
    except ValueError:
        print('Please enter a number')


def evaluate_triangle(lengths):
    if (
            lengths[0] > lengths[1] + lengths[2] or
            lengths[1] > lengths[0] + lengths[2] or
            lengths[2] > lengths[0] + lengths[1]
    ):
        print('No')
    else:
        print('Yes')


if __name__ == '__main__':
    sticks_and_triangles(sys.argv[1:])
