import pathlib
import sys


def print_dir(depth, directory):
    entry_counter = 0
    prefix = ''
    suffix = ''
    files = list(directory.glob('*'))
    for file in sorted(files):
        if entry_counter > 0:
            prefix = depth * ' '
        if entry_counter == len(files) - 1:
            suffix = '\n'

        if file.is_dir():
            prefix += 'D '
            print(prefix + str(file.name), end=' ')
            print_dir(depth + len(str(file.name)) + 3, file)
        elif file.is_file():
            prefix += 'F '
            print(prefix + str(file.name), end='\n' + suffix)
        entry_counter += 1


if __name__ == '__main__':
    if len(sys.argv) == 2:
        path = sys.argv[1]
    else:
        path = '.'

    print_dir(0, pathlib.Path(path))
