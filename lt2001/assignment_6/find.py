import pathlib
import sys


def find(arguments):
    try:
        check_argument_number(arguments)

        directory = pathlib.Path(arguments[0])
        check_path(directory)

        filename = arguments[1]
        check_filename(filename)

        print_filenames(directory, filename)
    except Exception as e:
        sys.stderr.write(str(e))


def check_argument_number(arguments):
    if len(arguments) != 2:
        raise Exception('Please use find.py with exactly two arguments!')


def check_path(path):
    if not path.is_dir():
        raise NotADirectoryError('First argument is no directory')


def check_filename(filename):
    if len(filename) == 0:
        raise Exception('Filename may not be empty')
    if filename.find('/') != -1:
        raise Exception('Filename may not contain a /')


def print_filenames(directory, filename):
    files = directory.rglob(filename)
    for file in files:
        sys.stdout.write(str(file) + '\n')


if __name__ == '__main__':
    find(sys.argv[1:])
