import sys


def read_file(filename):
    output = []
    with open(filename, encoding='utf8') as file:
        for lines in file:
            output.append(lines.strip().split(sep=' '))

    return output


def n_grams(n, text):
    n_grams_dict = {}
    for line in text:
        for number in range(2, n + 1):
            for word in range(0, len(line) - number + 1):
                ngram = tuple(line[word:word + number])
                if ngram in n_grams_dict.keys():
                    n_grams_dict[ngram] += 1
                else:
                    n_grams_dict[ngram] = 1
    return n_grams_dict


def ngr_counts(ngram_dictionary):
    separated_ngrams = separate_ngrams(ngram_dictionary)
    separated_occurrences = separate_occurrences(separated_ngrams)
    multiple_separated_occurrences = remove_single_occurrence(separated_occurrences)

    if len(multiple_separated_occurrences) != 0:
        highest_ngrams = get_highest_ngrams(multiple_separated_occurrences)
    else:
        highest_ngrams = []
    return highest_ngrams


def get_highest_ngrams(separated_occurrences):
    max_n = max(separated_occurrences)
    max_occurrence = max(separated_occurrences[max_n])
    return separated_occurrences[max_n][max_occurrence]


def remove_single_occurrence(separated_occurrences):
    separated_occurrences = {
        n: {
            occurrence: ngram_list
            for occurrence, ngram_list in occurrences.items() if occurrence != 1
        } for n, occurrences in separated_occurrences.items()
    }

    return {
        n: occurrences for n, occurrences in separated_occurrences.items() if occurrences != {}
    }


def separate_occurrences(separated_ngrams):
    separated_occurrences = {}
    for n, same_length_ngrams in separated_ngrams.items():
        if n not in separated_occurrences.keys():
            separated_occurrences[n] = {}
        for ngram, occurrence in same_length_ngrams:
            if occurrence not in separated_occurrences[n]:
                separated_occurrences[n][occurrence] = [ngram]
            else:
                separated_occurrences[n][occurrence].append(ngram)

    return separated_occurrences


def separate_ngrams(ngram_dictionary):
    separated_ngrams = {}
    for ngram, occurrences in ngram_dictionary.items():
        n = len(ngram)
        if n in separated_ngrams.keys():
            separated_ngrams[n].append([ngram, occurrences])
        else:
            separated_ngrams[n] = [[ngram, occurrences]]
    return separated_ngrams


if __name__ == '__main__':
    if len(sys.argv) == 2:
        filename = sys.argv[1]
    else:
        filename = 'tokenized.txt'

    file = read_file(filename)
    max_n = 5

    while True:
        repeated_ngrams = ngr_counts(n_grams(max_n, file))
        if len(repeated_ngrams[0]) < max_n:
            break
        max_n += 5

    print(repeated_ngrams)
