from collections import defaultdict

import nltk


def train_most_likely_tag(training_data):
    tags_for_words = defaultdict(dict)
    for word, tag in training_data:
        if word not in tags_for_words.keys():
            tags_for_words[word] = {tag: 1}
        else:
            if tag not in tags_for_words[word].keys():
                tags_for_words[word][tag] = 1
            else:
                tags_for_words[word][tag] += 1

    predictions = {}
    for word, tag in tags_for_words.items():
        tag = max(tag, key=lambda x: tag[x])
        predictions[word] = tag

    return predictions


def calculate_accuracy(data, predictions):
    DEFAULT_TAG = 'NN'

    correct_predictions = 0
    for word, tag in data:
        if word in predictions:
            predicted_tag = predictions[word]
        else:
            predicted_tag = DEFAULT_TAG

        if tag == predicted_tag:
            correct_predictions += 1

    total_accuracy = correct_predictions / len(data)
    return total_accuracy


if __name__ == '__main__':
    tagged_data = nltk.corpus.brown.tagged_words()
    separator = int(len(tagged_data) * 0.8)
    training_data = tagged_data[:separator]
    test_data = tagged_data[separator:]

    tag_predictions = train_most_likely_tag(training_data)
    print(calculate_accuracy(test_data, tag_predictions))
