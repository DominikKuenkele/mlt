import stanza

sentences = [
    {'en': 'I buy ten cars', 'de': 'Ich kaufe zehn Autos'},
    {'en': 'His sister bakes a cake', 'de': 'Seine Schwester backt einen Kuchen'},
    {'en': 'The painter is showing the picture', 'de': 'Der Maler zeigt das Bild'},
    {'en': 'We offer them support', 'de': 'Wir bieten ihnen Unterstützung an'},
    {'en': 'Two actors sell their house', 'de': 'Zwei Schauspieler verkaufen ihr Haus'}
]

translations = []
for index, translation in enumerate(sentences):
    trans = [[], []]
    dependencies = {'nsubj': {}, 'obj': {}}
    for lang, sentence in translation.items():
        nlp = stanza.Pipeline(lang=lang, processors='tokenize,mwt,pos,lemma,depparse')
        doc = nlp(sentence)
        for word in doc.sentences[0].words:
            if word.deprel == 'nsubj':
                trans[0].append(word.text)
            elif word.deprel == 'obj':
                trans[1].append(word.text)
    translations.append(trans)

# The words are the direct translations from English to German.
print(translations)
