class KeyValue:
    def __init__(self, key, value):
        self.key = key
        self.value = value

    def get_formatted_string(self):
        return f"keyval('{self.key}','{self.value}')"

    def __repr__(self):
        return self.key + ': ' + self.value
