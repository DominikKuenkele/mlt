import enum
import re

import inflect

from KeyValue import KeyValue


class NWRParser:
    class NWRType(enum.Enum):
        Additional = 0
        Central = 1
        Search = 2

    def __init__(self, string, nwr_type, knowledge):
        self.string = string
        self.nwr_type = nwr_type
        self.knowledge = knowledge
        self.inflect_engine = inflect.engine()

    def get_nwrs(self):
        conjunction_nwrs = []
        conjunctions = re.split(r'\band\b', self.string)
        for conjunction in conjunctions:
            node_way_relations = []

            conjunction = conjunction.strip()
            search_finished = False

            for location_type in self.knowledge['nwr']:
                if self.knowledge['nwr'][location_type] is not None:
                    nwrs, search_finished = self.__match_location_values(conjunction, location_type,
                                                                         self.knowledge['nwr'][location_type])
                    node_way_relations.extend(nwrs)

                if not search_finished:
                    nwrs, search_finished = self.__match_location_type(conjunction, location_type, self.nwr_type)
                    node_way_relations.extend(nwrs)

                if search_finished:
                    break

            if not search_finished:
                if conjunction != 'places':
                    node_way_relations.append(KeyValue('name', conjunction))

            conjunction_nwrs.append(node_way_relations)

        for index, nwr in enumerate(conjunction_nwrs):
            conjunction_nwrs[index] = self.__sort(nwr)

        return conjunction_nwrs

    def __match_location_values(self, string, location_type, location_type_dict):
        nwrs = []
        search_finished = False
        if isinstance(location_type_dict, dict) and 'values' in location_type_dict:
            for value_dict in location_type_dict['values']:
                value, value_attributes = list(value_dict.items())[0]
                value = str(value)

                if isinstance(value_attributes, dict) and 'synonyms' in value_attributes:
                    value_list = [synonym.replace('_', ' ')
                                  for synonym in value_attributes['synonyms']]
                else:
                    value_list = [value.replace('_', ' ')]
                value_list.extend([self.inflect_engine.plural_noun(singular, 0)
                                   for singular in value_list])

                regex = rf"(.*?)\b({'|'.join(value_list)})\b(.*?)"
                regex_search = re.search(regex, string)
                if regex_search is not None:
                    if isinstance(value_attributes, dict) and 'relations' in value_attributes:
                        relations, search_finished = self.__build_relations(regex_search,
                                                                            value_attributes['relations'])
                        nwrs.extend(relations)
                    if not search_finished:
                        nwrs.append(KeyValue(location_type, value))
                        search_finished = True

        return nwrs, search_finished

    def __sort(self, node_way_relations):
        node_nwrs = []
        leaf_nwrs = []
        for nwr in node_way_relations:
            if nwr.key in self.knowledge['nwr'] or nwr.key == 'name':
                node_nwrs.append(nwr)
            else:
                leaf_nwrs.append(nwr)
        node_nwrs.extend(leaf_nwrs)
        return node_nwrs

    def __build_relations(self, regex_search, location_value_relations):
        relations = []
        for relation, relation_attributes in location_value_relations.items():
            position = relation_attributes['position']
            relation_type = relation_attributes['type']
            relation_value = ''

            if position == 'before':
                relation_value = regex_search.group(1).strip()
            elif position == 'after':
                relation_value = regex_search.group(3).strip()
            elif position == 'self':
                matched_word = self.inflect_engine.singular_noun(regex_search.group(2).strip(), 1)
                relation_value = self.knowledge['key_value_maps'][relation][matched_word]

            if relation != 'name':
                relation_value = relation_value.lower()
                if relation_value == relation:
                    relation_value = 'yes'

            if relation_value != '':
                relations.append(KeyValue(relation, relation_value))

            return relations, relation_type == 'exclusive' and len(relations) > 0

    def __match_location_type(self, string, location_type, nwr_type):
        nwrs = []

        location_list = [location_type.replace('_', ' ')]
        if isinstance(self.knowledge['nwr'][location_type], dict) \
                and 'synonyms' in self.knowledge['nwr'][location_type]:
            location_list.extend([synonym.replace('_', ' ')
                                  for synonym in self.knowledge['nwr'][location_type]['synonyms']])
        location_list.extend([self.inflect_engine.plural_noun(singular, 0)
                              for singular in location_list])
        regex = rf"({'|'.join(location_list)})"
        regex_search = re.search(regex, string)
        if regex_search is not None:
            if nwr_type == NWRParser.NWRType.Additional:
                nwrs.append(KeyValue(location_type, 'yes'))
            else:
                nwrs.append(KeyValue(location_type, '*'))

        return nwrs, len(nwrs) > 0
