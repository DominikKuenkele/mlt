import enum
from typing import List
from KeyValue import KeyValue


class Query:
    class Distance(enum.Enum):
        NO_DIST = 0
        DIST_INTOWN = 1
        WALKING_DIST = 2

    def __init__(self):
        self.area = None
        self.central_nwrs = []  # type: List[List[KeyValue]]
        self.search_nwrs = []  # type: List[List[KeyValue]]
        self.additional_nwrs = []  # type: List[List[KeyValue]]
        self.query_type = ''
        self.distance = self.Distance.NO_DIST
        self.closest = False

    def get_query(self):
        area = f"area({self.area.get_formatted_string()})"
        self.central_nwrs = sorted(self.central_nwrs, key=lambda x: x[0].value if x != [] else '')
        central_nwr = self.__build_conjunction(self.central_nwrs, self.additional_nwrs)

        if len(self.search_nwrs) == 0:
            query = f"query({area},{central_nwr},qtype({self.query_type}))"
        else:
            search_nwr = self.__build_conjunction(self.search_nwrs)

            if isinstance(self.distance, self.Distance):
                distance = self.distance.name
            else:
                distance = self.distance

            query = f"query(around(center({area},{central_nwr}),search({search_nwr})," \
                    f"maxdist({distance}){',topx(1)' if self.closest else ''}),qtype({self.query_type}))"

        return query

    def __build_conjunction(self, nwrs, additional_nwrs=[]):
        nwr_strings = []
        for nwr_conjunction in nwrs:
            nwr_list = []
            for additional_nwr in additional_nwrs:
                nwr_conjunction.extend(additional_nwr)
            for nwr in nwr_conjunction:
                nwr_list.append(nwr.get_formatted_string())

            nwr_strings.append(f"{','.join(nwr_list)}")

        if len(nwr_strings) > 1:
            nwr_string = f"nwr(and({','.join(nwr_strings)}))"
        else:
            nwr_string = f"nwr({nwr_strings[0]})"
        return nwr_string
