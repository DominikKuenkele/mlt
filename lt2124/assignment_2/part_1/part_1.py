import yaml

from QueryParser import QueryParser


def load_data(filename):
    data = []
    with open(filename, encoding='utf8') as file:
        for line in file:
            data.append(line.strip().split('\t'))
    return data


def load_yml(filename):
    with open(filename) as file:
        content = yaml.safe_load(file)
    return content


def test_parser(data, knowledge):
    correct = 0
    for line in data:
        query_parser = QueryParser(line[0], knowledge)
        parsed_query = query_parser.parse()

        if parsed_query == line[1]:
            correct += 1

    return correct / len(data)


def parse_queries(queries, knowledge):
    parsed = []
    for query in queries:
        parser = QueryParser(query, knowledge)
        parsed.append((query, parser.parse()))
    return parsed


if __name__ == '__main__':
    knowledge = load_yml('knowledge.yml')
    data = load_data('nlmaps.tsv')

    print('Accuracy: {:2.2f}%'.format(test_parser(data, knowledge) * 100))

    pois = ['Japanese restaurants', 'Indian restaurants', 'Italian restaurants',
            'Starbucks', 'bakeries', 'banks', 'bus stops', 'butchers',
            'camp sites', 'cemeteries', 'charging stations', 'fire brigades',
            'fire hydrants', 'helipads', 'hiking maps', 'hospitals', 'kindergartens',
            'museums', 'peaks', 'playgrounds', 'post offices', 'schools', 'supermarkets']
    locations = ['Stockholm', 'Copenhagen', 'Helsinki', 'Oslo', 'Gothenburg', 'Malmö',
                 'Tampere', 'Aarhus', 'Turku', 'Bergen', 'Reykjavik']
    test_queries = [f"Where are {poi} in {loc}?" for poi in pois for loc in locations]

    # All queries are translated correctly
    parsed_queries = parse_queries(test_queries, knowledge)
    got_translated = 0
    for query in parsed_queries:
        if query[0] != '':
            got_translated += 1
    print('Portion of translations: {:2.2f}%'.format(got_translated / len(parsed_queries) * 100))
