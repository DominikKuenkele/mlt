import re

from KeyValue import KeyValue
from NWRParser import NWRParser
from Query import Query


class QueryParser:

    def __init__(self, query_string, knowledge):
        self.query_string = query_string
        self.knowledge = knowledge
        self.query = Query()

    def parse(self):
        regex = r'(Where are )(.*?)(with .*?)?(that .*?)?(wh?ere .*?)?(in which .*?)?(close to .*?)?(\bin\b .*?)\?'
        regex_search = re.search(regex, self.query_string)

        self.__parse_query_type(regex_search.group(1))
        self.__parse_main_string(regex_search.group(2), regex_search.group(7) is None)
        self.__parse_with(regex_search.group(3))
        self.__parse_that(regex_search.group(4))
        self.__parse_where(regex_search.group(5))
        self.__parse_in_which(regex_search.group(6))
        self.__parse_close_to(regex_search.group(7))
        self.__parse_area(regex_search.group(8))

        return self.query.get_query()

    def __parse_query_type(self, string):
        if string == 'Where are ':
            self.query.query_type = 'latlong'

    def __parse_main_string(self, string, is_central):
        regex = r'(the closest.*?)from (the )?(.*)'
        regex_search = re.search(regex, string)
        if regex_search is None:
            if is_central:
                nwr_parser = NWRParser(string, NWRParser.NWRType.Central, self.knowledge)
                self.query.central_nwrs.extend(nwr_parser.get_nwrs())
            else:
                nwr_parser = NWRParser(string, NWRParser.NWRType.Search, self.knowledge)
                self.query.search_nwrs.extend(nwr_parser.get_nwrs())
        else:
            search_nwr_parser = NWRParser(regex_search.group(1).replace(r'the closest', '').strip(),
                                          NWRParser.NWRType.Search,
                                          self.knowledge)
            self.query.search_nwrs.extend(search_nwr_parser.get_nwrs())

            central_nwr_parser = NWRParser(regex_search.group(3).strip(),
                                           NWRParser.NWRType.Central,
                                           self.knowledge)
            self.query.central_nwrs.extend(central_nwr_parser.get_nwrs())

            self.query.distance = Query.Distance.DIST_INTOWN
            self.query.closest = True

    def __parse_area(self, string):
        self.query.area = KeyValue('name', string.split('in ')[1])

    def __parse_that(self, string):
        if string is not None:
            string = string.removeprefix('that ').strip()
            nwr_parser = NWRParser(string, NWRParser.NWRType.Additional, self.knowledge)
            self.query.additional_nwrs.extend(nwr_parser.get_nwrs())

    def __parse_where(self, string):
        if string is not None:
            pattern = r'wh?ere ((.*?)and have (.*)|.*)'
            regex_search = re.search(pattern, string)

            if regex_search.group(2) is None:
                nwr_parser = NWRParser(regex_search.group(1), NWRParser.NWRType.Additional, self.knowledge)
                self.query.additional_nwrs.extend(nwr_parser.get_nwrs())
            else:
                nwr_parser = NWRParser(regex_search.group(2), NWRParser.NWRType.Additional, self.knowledge)
                self.query.additional_nwrs.extend(nwr_parser.get_nwrs())
                self.__parse_with('with ' + regex_search.group(3))

    def __parse_with(self, string):
        if string is not None:
            pattern = r'with ((.*?)(close by|no further than (\d*)(.*?) away)|.*)'
            regex_search = re.search(pattern, string)
            if regex_search.group(3) is None:
                nwr_parser = NWRParser(regex_search.group(1), NWRParser.NWRType.Additional, self.knowledge)
                self.query.additional_nwrs.extend(nwr_parser.get_nwrs())
            else:
                nwr_parser = NWRParser(regex_search.group(2), NWRParser.NWRType.Search, self.knowledge)
                self.query.search_nwrs.extend(nwr_parser.get_nwrs())

                if regex_search.group(3).startswith('close by'):
                    self.query.distance = Query.Distance.WALKING_DIST
                elif regex_search.group(3).startswith('no further'):
                    if regex_search.group(5) == 'm':
                        self.query.distance = regex_search.group(4)
                    elif regex_search.group(5) == 'km':
                        self.query.distance = int(regex_search.group(4)) * 1000
                    else:
                        self.query.distance = Query.Distance.WALKING_DIST

    def __parse_close_to(self, string):
        if string is not None:
            string = string.removeprefix('close to ').strip()

            nwr_parser = NWRParser(string, NWRParser.NWRType.Central, self.knowledge)
            self.query.central_nwrs.extend(nwr_parser.get_nwrs())

            self.query.distance = Query.Distance.DIST_INTOWN

    def __parse_in_which(self, string):
        if string is not None:
            string = string.removeprefix('in which ').strip()
            nwr_parser = NWRParser(string, NWRParser.NWRType.Additional, self.knowledge)
            self.query.additional_nwrs.extend(nwr_parser.get_nwrs())
