from nltk import grammar, parse

g = grammar.FeatureGrammar.fromstring("""
## Non-terminals
# Sentence:
S -> NP[num=?n] VP[num=?n]

# Verb Phrase
VP[tense=?t, num=?n] -> IVerb[tense=?t, num=?n]
VP[tense=?t, num=?n] -> IVerb[tense=?t, num=?n] PP
VP[tense=?t, num=?n] -> TVerb[tense=?t, num=?n] NP
VP[tense=?t, num=?n] -> TVerb[tense=?t, num=?n] NP PP

# Noun Phrase
NP[loc=?l, num=?n] -> Det[phon=?p] Nom[phon=?p, loc=?l, num=?n]
NP[loc=?l, num=?n] -> Det[phon=?p] Nom[phon=?p, loc=?l, num=?n] PP
NP[num=?n] -> PropN[num=?n]

# Prepositional Phrase
PP -> P[loc=?l] NP[loc=?l]

## Terminals a.k.a. Lexicon
# Verbs
IVerb[tense='past'] -> 'walked'
TVerb[tense='past'] -> 'saw' | 'ate' | 'chased'
TVerb[tense='pres', num='pl'] -> 'see' | 'chase'
TVerb[tense='pres', num='sg'] -> 'sees'

# Determiners
Det[phon='c'] -> 'the' | 'a'
Det[phon='v'] -> 'the' | 'an'

## Nominals
Nom[phon='c', num='sg'] -> 'dog' | 'man' | 'lunatic' | 'cat'
Nom[phon='c', loc='on'] -> 'grass'
Nom[phon='c', loc='in'] -> 'garden'
Nom[phon='v'] -> 'apple'
PropN[num='sg'] -> 'Mehdi'

# Preposition
P[loc='in'] -> 'in'
P[loc='on'] -> 'on'

""")

parser = parse.FeatureEarleyChartParser(g)

## Modify the grammar above to account for grammatical/ungrammatical sentences
sentences = [
    # grammatical
    "Mehdi walked in the garden",
    "Mehdi sees the lunatic on the grass",
    "a dog chased a cat",
    "a lunatic ate an apple",
    # ungrammatical
    "Mehdi see the lunatic in the garden",
    "Mehdi saw the lunatic in the grass",
    "a dog chase a cat",
    "a lunatic ate a apple",
]

for raw in sentences:
    print("====")
    print(raw)
    sentence = raw.split()  # tokenize
    trees = parser.parse(sentence)
    count = 0
    for tree in trees:
        count += 1
        print(count, tree)
        #tree.draw()
        print("----")
    if count == 0:
        # there shouldn't be any parse tree for ungrammatical sentences.
        print('THERE IS NO PARSE TREE!')
