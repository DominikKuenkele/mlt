from nltk.corpus import treebank
from nltk.grammar import ProbabilisticProduction
from nltk import PCFG, Nonterminal, ViterbiParser


def calculate_frequencies(parsed_sentences):
    frequencies = {}
    for tree in parsed_sentences:
        tree.chomsky_normal_form()
        productions = tree.productions()
        for rule in productions:
            alpha = rule.lhs()
            beta = rule.rhs()

            if alpha in frequencies:
                frequencies[alpha][beta] = frequencies.get(alpha, {}).get(beta, 0) + 1
            else:
                frequencies[alpha] = {beta: 1}
    return frequencies


def create_prob_production_rules(frequencies):
    probabilistic_production_rules = []
    for alpha in frequencies:
        sum_alpha = sum(count for count in frequencies[alpha].values())
        for beta, count in frequencies[alpha].items():
            prob = count / sum_alpha
            probabilistic_production_rules.append(ProbabilisticProduction(lhs=alpha, rhs=beta, prob=prob))
    return probabilistic_production_rules


if __name__ == '__main__':
    rule_frequencies = calculate_frequencies(treebank.parsed_sents()[:100])
    production_rules = create_prob_production_rules(rule_frequencies)
    grammar = PCFG(Nonterminal('S'), production_rules)
    viterbi_parser = ViterbiParser(grammar)
    for sentence in treebank.sents()[:5]:
        for tree in viterbi_parser.parse(sentence):
            print(tree)
