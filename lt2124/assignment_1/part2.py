import os
import sys
### YOU CAN ADD STANDARD PYTHON MODULE IMPORTS HERE IF YOU NEED THEM
from dateutil.relativedelta import relativedelta


def displayArcs(arcs, sentence):
    for arc in arcs:
        h, d, l = arc
        print(sentence[h]["form"], "---" + l + "-->", sentence[d]["form"])
    print()


def displayOriginalArcs(sentence):
    for x in sorted(sentence):
        if sentence[x]["head"] > 0:
            print(sentence[sentence[x]["head"]]["form"], "---" + sentence[x]["deprel"] + "-->", sentence[x]["form"])
    print()


### YOU WILL MODIFY THIS FUNCTION
def parse_arc_eager(sentence, transition_list = None):
    stack = []
    buffer = [x for x in sorted(sentence)]
    arcs = []
    while buffer != []:
        # pick the first transition
        t = transition_list.pop(0)
        if t.startswith("SHIFT"):
            stack.append(buffer.pop(0))

        elif t.startswith("LEFT-ARC"):
            label = t.removeprefix("LEFT-ARC-")
            head = buffer[0]
            dependent = stack.pop()
            arcs.append((head, dependent, label))

        elif t.startswith("RIGHT-ARC"):
            label = t.removeprefix("RIGHT-ARC-")
            head = stack[-1]
            dependent = buffer.pop(0)
            arcs.append((head, dependent, label))

            stack.append(head)

        elif t.startswith("REDUCE"):
            stack.pop()

    return arcs

    
def test_parse():
    sentence = {
        1: {"form": "the"},
        2: {"form": "cat"},
        3: {"form": "sits"},
        4: {"form": "on"},
        5: {"form": "the"},
        6: {"form": "mat"},
        7: {"form": "today"}
    }
    
    transition_sequence = ["SHIFT", "LEFT-ARC-det", "SHIFT", "LEFT-ARC-nsubj", "SHIFT", "SHIFT", "SHIFT", "LEFT-ARC-det", "LEFT-ARC-case", "RIGHT-ARC-nmod", "REDUCE", "RIGHT-ARC-advmod", "REDUCE"]
    
    arcs = parse_arc_eager(sentence, transition_list=transition_sequence)
    displayArcs(arcs, sentence)



if __name__ == "__main__":
    test_parse()
