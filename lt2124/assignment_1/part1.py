import sys
import os
import argparse
import math
from loader import load_dir


def load_data(rootdir):
    '''
    Loads all the data by searching the given directory for
    subdirectories, each of which represent a class.  Then returns a
    dictionary of class vs. text document instances.
    '''
    classdict = {}
    for classdir in os.listdir(rootdir):
        fullpath = os.path.join(rootdir, classdir)
        print("Loading from {}.".format(fullpath))
        if os.path.isdir(fullpath):
            classdict[classdir] = load_dir(fullpath)

    return classdict


def calc_prob(classdict, classname, ngram):
    '''
    Calculates p(classname|word) given the corpus in classdict.
    '''
    if classname not in classdict.keys():
        raise Exception('Class "{}" not found in data'.format(classname))

    all_songs = [song
                 for songs in classdict.values()
                 for song in songs]
    all_songs_as_ngrams = [list(zip(*[song[i:]
                                      for i in range(len(ngram))]))
                           for song in all_songs]
    class_songs_as_ngrams = [list(zip(*[song[i:]
                                        for i in range(len(ngram))]))
                             for song in classdict[classname]]

    number_of_all_songs_containing_ngram = len([song
                                                for song in all_songs_as_ngrams
                                                if ngram in song])
    number_of_songs_in_class_containing_ngram = len([song
                                                     for song in class_songs_as_ngrams
                                                     if ngram in song])

    if number_of_all_songs_containing_ngram == 0:
        raise Exception('No song contains the ngram {}'.format(ngram))

    p_class_given_word = number_of_songs_in_class_containing_ngram / number_of_all_songs_containing_ngram

    return p_class_given_word


if __name__ == "__main__":

    '''
    Entry point for the code. We load the command-line arguments.
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument("filesdir", help="The root directory containing the class directories and text files.")
    parser.add_argument("classname", help="The class of interest.")
    parser.add_argument("feature", help="The ngram of interest for calculating -log2 p(class|feature).", nargs='+')

    args = parser.parse_args()

    if len(args.feature) > 2:
        raise Exception('Please specify maximum two features')

    ngram = tuple(args.feature)

    corpus = load_data(args.filesdir)

    print("Number of classes in corpus: {}".format(len(corpus)))

    print("Looking up probability of class {} given ngram {}.".format(args.classname, ngram))
    prob = calc_prob(corpus, args.classname, ngram)
    if prob == 0:
        print("-log2 p({}|{}) is undefined.".format(args.classname, ngram))
    else:
        print("-log2 p({}|{}) = {:.3f}".format(args.classname, ngram, -math.log2(prob)))
