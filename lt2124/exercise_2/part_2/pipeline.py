import argparse
import sys

import stanza


def parse_words(string, language, mode, show_pos, dep_rel, verbose, sentiment, constituency):
    output_str = ''
    nlp = stanza.Pipeline(lang=language, processors='tokenize,mwt,pos,lemma,depparse,sentiment,constituency')
    doc = nlp(string)
    for sentence_index, sentence in enumerate(doc.sentences, start=1):
        if verbose:
            output_str += f'====== Sentence {sentence_index} =======\n'
        for word_index, word in enumerate(sentence.words, start=1):
            descriptions = {
                'mode': '',
                'index': 'Word number: ',
                'deprel': 'Dependency relation: '
            }
            if mode == 'lemmas':
                descriptions['mode'] = 'Lemma: '

            if not verbose:
                for key in descriptions:
                    descriptions[key] = ''

            output = descriptions['index'] + str(word_index)
            if mode == 'lemmas':
                output += '\t' + descriptions['mode'] + word.lemma

            if show_pos:
                output += '_' + word.pos

            if dep_rel:
                output += '\t' + descriptions['deprel'] + word.deprel
            output_str += output + '\n'

        if constituency:
            output_str += f'|--> {sentence.constituency}\n'

        if sentiment:
            sentiment_descr = {
                0: 'negative',
                1: 'neutral',
                2: 'positive'
            }
            if verbose:
                output_str += f'|-->  Sentiment: {sentiment_descr[sentence.sentiment]}\n'
            else:
                output_str += f'|--> {sentence.sentiment}\n'

    return output_str.rstrip()


def build_deptree(string, language):
    nlp = stanza.Pipeline(lang=language, processors='tokenize,mwt,pos,lemma,depparse')
    doc = nlp(string)

    prefix_string = r'''
\documentclass[12pt]{article}
\usepackage{tikz-dependency}
\begin{document}
'''
    suffix_string = r'''
\end{document}
'''
    dependencies = ''
    for sentence in doc.sentences:
        dependencies += r'''\begin{dependency}
    \begin{deptext}[column sep=0.9cm]
'''
        words = r' \& '.join([word.text for word in sentence.words])
        dependencies += '\t\t' + words + r' \\' + '\n\t' + r'\end{deptext}' + '\n'

        for word in sentence.words:
            if word.deprel == 'root':
                dependencies += '\t' + fr'\deproot[edge unit distance=4ex]{{{word.id}}}{{{word.deprel}}}' + '\n'
            else:
                dependencies += '\t' fr'\depedge{{{word.head}}}{{{word.id}}}{{{word.deprel}}}' + '\n'

        dependencies += r'\end{dependency}' + '\n\n\n'

    return f'{prefix_string}{dependencies.strip()}{suffix_string}'.strip()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("language", help="The language of the sentence.")
    parser.add_argument("mode", help="The mode in which the sentence should pe parse (e.g. lemma).")
    parser.add_argument('--pos', action='store_true', help='Concatenates lemmas with the part of speech')
    parser.add_argument("--deprel", action='store_true', help="Shows dependency relation for each word")
    parser.add_argument("--verbose", action='store_true', help="Adds descriptions to output")
    parser.add_argument("--sentiment", action='store_true', help="Adds sentiment analysis for each sentence")
    parser.add_argument("--constituency", action='store_true', help="Displays constituency analysis for each sentence")
    args = parser.parse_args()

    if args.mode == 'lemmas':
        print(parse_words(sys.stdin.readline().rstrip(),
                          args.language,
                          args.mode,
                          args.pos,
                          args.deprel,
                          args.verbose,
                          args.sentiment,
                          args.constituency))
    elif args.mode == 'deptree':
        print(build_deptree(sys.stdin.readline().rstrip(), args.language))
