#!/bin/bash

mkdir -p "classes"

IFS=","
{
    read
    while read -r index1 index2 song year artist genre lyrics
    do
        mkdir -p "classes/$genre"
        echo "$lyrics" > "classes/$genre/$(printf %04d.txt $(($(find classes/$genre -type f | wc -l)+1000)))"
    done 
}< "$1"